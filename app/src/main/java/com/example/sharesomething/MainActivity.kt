package com.example.sharesomething

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.net.wifi.WifiManager
import android.content.Context.WIFI_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.Handler
import android.util.Log
import android.net.wifi.WifiConfiguration
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.content.Intent
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.net.Uri


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    private var mReservation: WifiManager.LocalOnlyHotspotReservation? = null
    private val TAG = "MAIN_ACTIVITY"

    @RequiresApi(Build.VERSION_CODES.O)
    private fun turnOnHotspot() {
        val manager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

        manager.startLocalOnlyHotspot(object : WifiManager.LocalOnlyHotspotCallback() {

            override fun onStarted(reservation: WifiManager.LocalOnlyHotspotReservation) {
                super.onStarted(reservation)
                Log.d(TAG, "Wifi Hotspot is on now")
                mReservation = reservation
            }

            override fun onStopped() {
                super.onStopped()
                Log.d(TAG, "onStopped: ")
            }

            override fun onFailed(reason: Int) {
                super.onFailed(reason)
                Log.d(TAG, "onFailed: ")
            }
        }, Handler())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun turnOffHotspot() {
        if (mReservation != null) {
            mReservation!!.close()
        }
    }


    //api < 26
    private fun showWritePermissionSettings(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            if (!Settings.System.canWrite(this)) {
                Log.v("DANG", " " + !Settings.System.canWrite(this))
                val intent = Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS)
                intent.data = Uri.parse("package:" + this.packageName)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                this.startActivity(intent)
                return false
            }
        }
        return true //Permission already given
    }


    fun setWifiEnabled(wifiConfig: WifiConfiguration, enabled: Boolean): Boolean {
        val wifiManager: WifiManager
        try {
            if (enabled) { //disables wifi hotspot if it's already enabled
                wifiManager.isWifiEnabled = false
            }

            val method = wifiManager.javaClass
                .getMethod(
                    "setWifiApEnabled",
                    WifiConfiguration::class.java,
                    Boolean::class.javaPrimitiveType
                )
            return method.invoke(wifiManager, wifiConfig, enabled) as Boolean
        } catch (e: Exception) {
            Log.e(this.javaClass.toString(), "", e)
            return false
        }

    }
}
