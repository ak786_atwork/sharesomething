package com.example.sharesomething

import android.content.Context
import android.net.wifi.WifiConfiguration
import android.content.Context.WIFI_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import android.net.wifi.WifiManager
import android.util.Log
import androidx.core.util.Preconditions
import java.lang.reflect.Method


class WifiApManager @Throws(SecurityException::class, NoSuchMethodException::class)
constructor(context: Context) {
    private val mWifiManager: WifiManager
    private val TAG = "Wifi Access Manager"
    private val wifiControlMethod: Method
    private val wifiApConfigurationMethod: Method
    private val wifiApState: Method
    val wifiApConfiguration: WifiConfiguration?
        get() {
            try {
                return wifiApConfigurationMethod.invoke(mWifiManager, null) as WifiConfiguration?
            } catch (e: Exception) {
                return null
            }

        }

    init {
        var context = context
        context = Preconditions.checkNotNull(context)
        mWifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifiControlMethod = mWifiManager.javaClass.getMethod(
            "setWifiApEnabled",
            WifiConfiguration::class.java,
            Boolean::class.javaPrimitiveType
        )
        wifiApConfigurationMethod = mWifiManager.javaClass.getMethod("getWifiApConfiguration", null)
        wifiApState = mWifiManager.javaClass.getMethod("getWifiApState")
    }

    fun setWifiApState(config: WifiConfiguration, enabled: Boolean): Boolean {
        var config = config
        config = Preconditions.checkNotNull(config)
        try {
            if (enabled) {
                mWifiManager.isWifiEnabled = !enabled
            }
            return wifiControlMethod.invoke(mWifiManager, config, enabled)
        } catch (e: Exception) {
            Log.e(TAG, "", e)
            return false
        }

    }

    fun getWifiApState(): Int {
        try {
            return wifiApState.invoke(mWifiManager) as Int
        } catch (e: Exception) {
            Log.e(TAG, "", e)
            return WIFI_AP_STATE_FAILED
        }

    }

    companion object {
        private val WIFI_AP_STATE_FAILED = 4
    }

    private fun changeStateWifiAp(activated: Boolean) {

        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiConfiguration wifiConfiguration = new WifiConfiguration();
        wifiConfiguration.SSID = "MyDummySSID";

        val method: Method
        try {
            method = wifiManager.getClass().getDeclaredMethod(
                "setWifiApEnabled",
                WifiConfiguration::class.java,
                java.lang.Boolean.TYPE
            )
            method.invoke(wifiManager, wifiConfiguration, activated)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}